# -*- coding: utf-8 -*-

from odoo import models, fields, api

class Cursos(models.Model):
    _name = 'appstudy.cursos'
    name     = fields.Char(required=True)
    alumnos  = fields.Many2many('res.partner')
    examenes = fields.One2many('appstudy.quiz','curso')

class Examen(models.Model):
	_name           = 'appstudy.quiz'
	name            = fields.Char(required=True,string="Nombre")
	preguntas       = fields.One2many('appstudy.pregun','quiz')
	curso           = fields.Many2one('appstudy.cursos') 
    
class Preguntas(models.Model):
	_name         = 'appstudy.pregun'
	name          = fields.Char(required=True,string="Nombre")
	quiz          = fields.Many2one('appstudy.quiz')
	respuesta     = fields.Char(required=True)
	alternativas  = fields.One2many('appstudy.alt', 'pregunta',required=True)
	imagen        = fields.Binary()

class Alternativa(models.Model):
	_name = 'appstudy.alt'
	name     = fields.Char(required=True)
	pregunta = fields.Many2one('appstudy.pregun')

class ContactoAppStudy(models.Model):
	_name       = 'res.partner'
	_inherit    = 'res.partner'
	passquiz    = fields.Char(string="Contraseña Quiz")
	
